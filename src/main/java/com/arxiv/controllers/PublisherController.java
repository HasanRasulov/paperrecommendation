package com.arxiv.controllers;

import java.security.Principal;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arxiv.entities.Publisher;
import com.arxiv.services.PublisherService;


@RestController
public class PublisherController {

			
	@Autowired
	PublisherService publisherService;

	
	@PostMapping("/signup")
	public String signup(@RequestBody Publisher user) {
				
		publisherService.save(user);
		
		return ""; //return home page
	}
	
	@PutMapping("/tag")
	public Publisher addTag(@RequestBody List<String> tags,Principal user) {
		
		publisherService.addTag(user.getName(), tags);
		
		return null;//return home page
	}
	
	@PutMapping("/link/{otherUsername}")
	public Publisher connectUsers(@PathVariable String otherUsername,Principal user) {
		
		publisherService.addConnection(user.getName(), otherUsername);
		
		return null; //home page;
	}
	
}
