package com.arxiv.controllers;

import com.arxiv.entities.*;
import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arxiv.services.RecommendationService;

@RestController
public class RecommendationController {

	
	
	@Autowired
	private RecommendationService service;
	
	
	@GetMapping("recommend")
	public List<Record> recommend(Principal user) {
		
		return service.recommend(user.getName());
		
	}
	
	
}
