package com.arxiv.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arxiv.entities.Paper;
import com.arxiv.repositories.PublisherRepository;
import com.arxiv.services.DataSourceService;
import com.arxiv.services.PublisherService;

@RestController
public class SearchController {

	@Autowired
	DataSourceService source;
	
	@Autowired
	PublisherService pubService;
	
	@Autowired
	PublisherRepository repo;
	
	
	@GetMapping("/search")
	public Paper searchByKeyword(@RequestParam("q") String keyword,Principal user) {
	    
		pubService.addHistory(user.getName(), keyword);	
		return source.searchForPapers(keyword);
	}
	
}
