package com.arxiv.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arxiv.entities.*;

@Service
public class RecommendationService {

	
	@Autowired
	private DataSourceService source;
	
	@Autowired
	private PublisherService publisherService;
	
	
	@Transactional
	public List<Record>  recommend(String username){
	
		List<Record> records = new ArrayList<Record>(); 
		
		Set<String> ids = publisherService.findByUsername(username).getRecommendations();		
		
		ids.forEach(id -> records.add(source.searchForPapersById(id)));
		
		return records;
		
	}
	
	
	
}
