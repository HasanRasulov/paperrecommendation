package com.arxiv.services;

import java.util.HashSet;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.arxiv.entities.*;
import com.arxiv.repositories.PublisherRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private PublisherRepository pubRepo;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Publisher pub = pubRepo.findByUsername(username);

		if (pub == null)
			throw new UsernameNotFoundException(username);

		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		for (Role role : pub.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
		}

		return new User(pub.getUsername(), pub.getPassword(), getGrantedAuthorities(pub));

	}

	public Set<GrantedAuthority> getGrantedAuthorities(Publisher pub) {

		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		for (Role role : pub.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
		}

		return grantedAuthorities;
	}

}
