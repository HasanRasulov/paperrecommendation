package com.arxiv.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.arxiv.entities.*;

@Service
public class DataSourceService {


	 @Autowired
	 private RestTemplate rest;
	 private static final String apiKey="547180a4962c91e52c805bcf75937d16";
	 private String url;
	 
	 public DataSourceService() {
	  
           url = "http://api.springer.com/metadata/json?api_key="+apiKey+"&q=";
	 }
	 
	 public Paper searchForPapers(String query) {
		 
		 return rest.getForObject(url + query,Paper.class);
		 
	 }
	 
	public Record searchForPapersById(String id) {

		return rest.getForObject(url + id, Paper.class).getRecords().get(0);
	}
	 
	
}
