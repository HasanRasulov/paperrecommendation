package com.arxiv.services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arxiv.entities.*;
import com.arxiv.repositories.PublisherRepository;
import com.arxiv.repositories.RoleRepository;

@Service
public class PublisherService {

	@Autowired
	private PublisherRepository publisherRepo;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private DataSourceService source;
	
	
	
	@Transactional
	public void addConnection(String username, String otherName) {
		Publisher user = publisherRepo.findByUsername(username);
		Publisher otherUser=publisherRepo.findByUsername(otherName);
		user.addConnection(otherUser.getId());
		addRecommendation(user,otherUser.getRecommendations());
	}
	
	@Transactional
	public void addHistory(String username, String entry) {
		Publisher user = publisherRepo.findByUsername(username);
		user.addHistory(entry);
				
		Paper paper = source.searchForPapers(entry);
		Set<String> recommendations=new HashSet<>();
		paper.getRecords().forEach(r -> recommendations.add(r.getDoi()));
		
		addRecommendation(user, recommendations);
		user.getConnections().forEach(id -> addRecommendation(publisherRepo.findById(id).get(),recommendations));
	}
	
	@Transactional
	public void addTag(String username, List<String> tag) {
		Publisher user = publisherRepo.findByUsername(username);
		user.addTag(tag);
		Set<String> recommendations=new HashSet<>();
	    tag.forEach(t -> source.searchForPapers(t).getRecords().forEach(r -> recommendations.add(r.getDoi())));
	    addRecommendation(user, recommendations);
	    user.getConnections().forEach(id -> addRecommendation(publisherRepo.findById(id).get(),recommendations));
	}

	private void addRecommendation(Publisher user, Set<String> paperId) {
		user.addRecommendation(paperId);
	}

	@Transactional
	public Publisher findByUsername(String username) {
		return publisherRepo.findByUsername(username);
	}
	
	@Transactional
	public void save(Publisher user) {
	    
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		Set<Role> allRoles = new HashSet<>(roleRepository.findAll());
		if(allRoles.isEmpty()) {
			allRoles.add(new Role("ROLE_ADMIN"));
			allRoles.add(new Role("ROLE_ADMIN"));
			roleRepository.save(allRoles);
		}
        user.setRoles(allRoles);
        publisherRepo.save(user);
	}
	
}
