package com.arxiv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import com.arxiv.entities.Paper;
import com.arxiv.entities.Publisher;
import com.arxiv.repositories.PublisherRepository;
import com.arxiv.services.DataSourceService;
import com.arxiv.services.PublisherService;
import com.arxiv.services.RecommendationService;
import com.arxiv.services.SecurityService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class PaperRecommmenderApplication implements CommandLineRunner{

	@Autowired
	private  DataSourceService controller;
	
	@Autowired
	private PublisherService pubser;
	
	@Autowired
	private RecommendationService rec;
	
	public static void main(String[] args) {
		SpringApplication.run(PaperRecommmenderApplication.class, args);
		
	}
	
	@Bean
	public RestTemplate getRestTemplate() {  
		 return new RestTemplate();
	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println(rec.recommend("Hasan_Rasulov"));
		
		
//		Paper res =controller.searchForPapers("A probabilistic interval-based event calculus for activity recognition");
		
	//	System.out.println(res);

	//	pubser.save(new Publisher("Nergiz Samadzade",(byte)23,"Nargiz_Samadzade","kibernetika"));
		
	}

	
}
