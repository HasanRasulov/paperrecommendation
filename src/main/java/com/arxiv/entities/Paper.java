package com.arxiv.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = false)
public class Paper{

	
	private String apiMessage;

	private String query;

	private String apiKey;

	@JsonProperty("result")
	private List<Result> result;

	private List<Record> records;

	private List<Facet> facets;

}