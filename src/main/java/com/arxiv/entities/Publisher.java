package com.arxiv.entities;


import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude= {"passwordConfirm","searchHistory","connections","recommendations"})
@Entity
public class Publisher implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
    
	@Column(unique=true)
	private String username;
	
	private String password;
	
	@Transient
    private String passwordConfirm;
	
	@ManyToMany
    private Set<Role> roles;
	
	private String fullName;

	private Byte age;

	@ElementCollection
	private Set<String> tags;

	@ElementCollection
	private Set<Long> connections;
	
	@ElementCollection
	private Set<String> searchHistory;
	
	@ElementCollection
	private Set<String> recommendations;
	
	public void addTag(List<String> tag) {
		tags.addAll(tag);
	}

	public void addHistory(String entry) {
		searchHistory.add(entry);
	}

	public void addConnection(Long id) {
	 	connections.add(id);
            
	}

	public void addRecommendation(Set<String> paperId) {
		recommendations.addAll(paperId);
	}

}
