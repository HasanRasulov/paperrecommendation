package com.arxiv.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Record {

	private String contentType;
	private String identifier;
	private List<PaperUrL> url;
	
	private String title;
	private List<Creator> creators;
	
	private String publicationName;
	private boolean openaccess;
	private String doi;
	private String publisher;
	private String publicationDate;
	private String publicationType;
	private String issn;
	
	@JsonProperty("volume")
	private Integer volume;
	
	private String number;
	private Integer startingPage;
	private Integer endingPage;
	private String journalId;
	private String copyright;
	
    @JsonProperty("abstract")
	private String abstracts;

	
}
