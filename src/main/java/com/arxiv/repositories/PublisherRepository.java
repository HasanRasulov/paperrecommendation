package com.arxiv.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.arxiv.entities.*;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long>{
      
	public Publisher findByUsername(String username);
	
}
