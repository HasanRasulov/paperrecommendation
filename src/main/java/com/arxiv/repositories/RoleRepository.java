package com.arxiv.repositories;

import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.arxiv.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

	public void save(Set<Role> roles);
	
}
